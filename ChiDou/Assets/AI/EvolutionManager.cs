﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvolutionManager : MonoBehaviour
{
    public int[] layerShape;
    public int populationSize;
    public GameObject agentPrefab;
    public Transform agentsParent;
    private GA ga;
    private List<Genome> genomeList = new List<Genome>();
    private List<NeuralNetwork> nnList = new List<NeuralNetwork>();
    private List<Agent> agentList = new List<Agent>();

    public bool loadWeights;
    public string weightPath;
    void Start()
    {
        InitTest();
    }

    void Update()
    {
        RunTest();
    }

    void SaveBest()
    {
        NeuralNetwork bestNn = null;
        double maxFit = -9999;
        foreach (var item in agentList)
        {
            if (item.ge.fitness > maxFit)
            {
                bestNn = item.nn;
                maxFit = item.ge.fitness;
            }
        }
        if (bestNn != null)
        {
            bestNn.SaveWeights(weightPath);
        }
    }

    void InitTest()
    {
        InitWorld();
        ga = new GA(populationSize);
        for (int i = 0; i < populationSize; i++)
        {
            NeuralNetwork nn = new NeuralNetwork(layerShape);
            if (loadWeights)
            {
                if (!nn.LoadWeights(weightPath))
                {
                    Debug.LogError("没有保存最佳权重，无法加载");
                }
            }
            else
            {
                nn.RandomWeights();
            }


            Genome ge = new Genome(nn.GetWeights(), 0, nn.splitPoints);

            Agent ac = Instantiate(agentPrefab).GetComponent<Agent>();
            ac.transform.SetParent(agentsParent);
            ac.SetInfo(nn, ge);
            ac.Reset();

            nnList.Add(nn);
            genomeList.Add(ge);
            agentList.Add(ac);
        }
    }

    void RunTest()
    {
        if(CheckAllDie())
        {
            ResetWorld();
            FinishGA();
        }
    }

    void FinishGA()
    {
        SaveBest();
        List<double[]> weightsList = ga.Run(genomeList);
        for (int i = 0; i < weightsList.Count; i++)
        {
            nnList[i].SetWeights(weightsList[i]);
            agentList[i].nn = nnList[i];
            genomeList[i] = new Genome(nnList[i].GetWeights(), 0, nnList[i].splitPoints);
            agentList[i].ge = genomeList[i];
            agentList[i].Reset();
        }
    }

    public virtual bool CheckAllDie()
    {
        bool isAllDie = true;
        foreach (var t in agentList)
        {
            if (t.gameObject.activeSelf)
            {
                isAllDie = false;
                break;
            }
        }
        return isAllDie;
    }

    public virtual void ResetWorld()
    {

    }

    public virtual void InitWorld()
    {

    }
}
