﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ev : EvolutionManager
{
    public GameObject douPre;
    public int douNum;
    public override void InitWorld()
    {
        for (int i = 0; i < douNum; i++)
        {
            Instantiate(douPre, new Vector3(Random.Range(-20f, 20f), 0, Random.Range(-20f, 20f)), Quaternion.identity);
        }
    }
}
